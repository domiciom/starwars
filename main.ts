import {Server} from './server/server'
import {planetsRouter} from './planets/planets.router'

const server = new Server()
server.bootstrap([planetsRouter]).then(server=>{
  console.log('Listening i am:', server.application.address())
}).catch(error=>{
  console.log('This is not the Jedi way')
  console.error(error)
  process.exit(1)
})
