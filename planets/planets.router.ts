import { ModelRouter } from '../common/model-router'
import * as restify from 'restify'
import {Planet} from './planets.model'

class PlanetsRouter extends ModelRouter<Planet> {
	
	constructor() {
		super(Planet)
	}

	findByName = (req, resp, next) => {
		if (req.query.name) {
			Planet.findByName(req.query.name)
				.then(planet => planet ? [planet] : [])
				.then(this.renderAll(resp, next))
				.catch(next)
		} else {
			next()
		}
	}

	applyRoutes(application: restify.Server){
		application.get('/planets', [this.findByName, this.findAll])
		application.get('/planets/:id', [this.validateId, this.findById]);		
		application.post('/planets', this.save);	
		application.put('/planets/:id', [this.validateId, this.replace]);
		application.patch('/planets/:id', [this.validateId, this.update])
		application.del('/planets/:id', [this.validateId, this.delete]);
	}
}

export const planetsRouter = new PlanetsRouter()