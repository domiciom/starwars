import * as mongoose from 'mongoose'
import {environment} from '../common/environment'
var swapi: any = require('swapi-node');

export interface Planet extends mongoose.Document{
	name: string,
	climate: string,
	terrain: string,
	films: number
}

export interface PlanetModel extends mongoose.Model<Planet> {
	findByName(name: string): Promise<Planet>
}

const planetSchema = new mongoose.Schema({
	name: {
		type: String,
		required: true,
		maxlength: 80,
		minlength: 3
	},
	climate: {
		type: String,
		require: true,
		minlength: 3
	},
	terrain: {
		type: String,
		required: true,
		minlength: 3
	},
	films :{
		type: Number,
		required: false,
		default: 0
	}
})

planetSchema.statics.findByName = function(name: string){
	return this.find({ "name": { $regex: name, $options: 'i' } })
}

const saveMiddleware = function (next) {
	const planet = (this as Planet)
	if (!planet.films) {
		swapi.get(environment.swapi.url+'planets/?search='+planet.name).then((result)=>{
			console.log('result', result);
			if(result.count == 1){
				planet.films = result.results[0].films.length
			}
			next()
		}).catch(next)
	}
}

planetSchema.pre('save', saveMiddleware)

export const Planet = mongoose.model<Planet, PlanetModel>('Planet', planetSchema)
Planet.ensureIndexes()
