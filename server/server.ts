import * as restify from 'restify'
import * as mongoose from 'mongoose'
import {environment} from '../common/environment'
import {Router} from '../common/router'
import {mergePatchBodyParser} from './merge-patch.parser'
import {handleError} from './error.handler'

export class Server {

  application: restify.Server
  
  initializeDb(){
	return mongoose.connect(environment.db.url, {
		useNewUrlParser: true
	})
  }

  initRoutes(routers: Router[]): Promise<any>{
    return new Promise((resolve, reject)=>{
      try{

        this.application = restify.createServer({
          name: 'starwars',
          version: '1.0.0'
        })

        this.application.use(restify.plugins.queryParser())
        this.application.use(restify.plugins.bodyParser())
        this.application.use(mergePatchBodyParser)

        //routes
		for (let router of routers){
			router.applyRoutes(this.application)
		}

        this.application.get('/info', [
          (req, resp, next)=>{
            if(req.userAgent() && req.userAgent().includes('MSIE 7.0')){
            let error: any = new Error()
            error.statusCode = 400
            error.message = 'Please, update your browser'
            return next(error)
          }
          return next()
        },(req, resp, next)=>{
          resp.json({
            browser: req.userAgent(),
            method: req.method,
            url: req.href(),
            path: req.path(),
            query: req.query
          })
          return next()
        }])

        this.application.listen(environment.server.port, ()=>{
           resolve(this.application)
        })
		
		this.application.on('restifyError', handleError)

      }catch(error){
        reject(error)
      }
    })
  }

  bootstrap(routers: Router[] = []): Promise<Server>{
      return this.initializeDb().then(()=>
		  this.initRoutes(routers).then(()=> this)  
	  ) 
  }

}
